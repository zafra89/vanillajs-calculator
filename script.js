let num1 = document.querySelector(".num1");
let num2 = document.querySelector(".num2");
let calculateBtn = document.querySelector(".calculate-btn");
let result = document.querySelector(".result");
let errorMsg = document.querySelector(".error-msg");
let decimalCheck = document.querySelector(".decimal");

calculateBtn.addEventListener('click', calculator);
initialize();

function calculator() {
  var parsedNum1, parsedNum2, operator, writtenDecimal;

  if (decimalCheck.checked) {
    parsedNum1 = parseFloat(num1.value);
    parsedNum2 = parseFloat(num2.value);
  } else {
    parsedNum1 = parseInt(num1.value);
    parsedNum2 = parseInt(num2.value);
  }

  checkIfDecimal(parsedNum1, parsedNum2, writtenDecimal);

  operatorCalc(parsedNum1, parsedNum2, operator);
}

function initialize() {
  num1.value = "0";
  num2.value = "0";
  operator.value = "";
  errorMsg.textContent = ``;
}

function checkIfDecimal(n1, n2, dec) {
  dec = document.querySelector(".writtenDecimal");
  if (Number.isInteger(n1) && Number.isInteger(n2)) {
    dec.checked = false;
  } else {
    dec.checked = true;
  }
}

function operatorCalc(n1, n2, oper) {
  oper = document.querySelector(".operator").value;

  if ( ! isNaN(n1) && ! isNaN(n2) && oper != "") {
    if (oper === "+") {
      result.value = n1 + n2;
    } else if (oper === "-") {
      result.value = n1 - n2;
    } else if (oper === "*") {
      result.value = n1 * n2;
    } else if (oper === "/") {
      result.value = n1 / n2;
    }
    initialize();
  } else {
    errorMsg.textContent = `Escribe solo números o comprueba si has elegido un operador.`
  }
}